#-*- coding: utf-8 -*-
#to compile : python -Qnew multipleConversion.py

import easyConversion
import textatistic

c = easyConversion.Conversion()

#Conversion of the corpus
c.convertAll()

#Print
c.printAll()
c.printAverage()

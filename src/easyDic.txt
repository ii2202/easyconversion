abide : remain
abstracted : absentminded
acclamation : applause
acerbity : bitterness
acquire : get
acrimony : harshness
acute : sharp
adhere : stick
adult : grown-up
adventitious : incidental
aeroplane : plane
affect : sentiment
affix : add
affluent : wealthy
aged : old
agreeable : nice
agriculture : farming
ailment : sickness
alacrity : promptness
alcoholic : sot
a little : a bit
allay : lessen
alleviate : lighten
allocate : allot
also : too
alter : change
altruism : unselfishness
ambitious : pushy
ameliorate : improve
amiable : friendly
am not : ain't
anathema : curse
anesthetized : numb
annoy : rile
annually : every year
anomie : appathy
anyone : anybody
anywhere : anyplace
aperture : gap
apparel : clothing
appeal : request
appear : show up
apprehend : catch
apprehension : worry
approbation : sanction
approximately : kind of
approximatively : around
arcane : obscure
archaic : old-fashioned
arduous : difficult
are not : aren't
arrest : nab
ascend : climb
ascertain : determine
asperity : sourness
aspersion : slander
assembly : conference
assiduous : diligent
assignment : stint
assistant : helper
assist : help
associate : coworker
assure : promise
astounded : flabbergasted
augur : foretell
authorize : let
avaricious : greedy
aver : assert
avocation : hobby
award : prize
barter : swap
beatitude : blessedness
begin : start
believe : guess
beneficial : good
benevolence : kindness
burdensome : heavy
calamity : disaster
cannot : can't
caress : hug
categorize : pigeonhole
cease : stop
characteristic : attribute
chide : scold
child : kid
circumlocution : indirectness
circumscribe : surround
clamorous : loud
cogitate : think
cognizant : aware
colloquy : discussion
comic : funny
commence : start
commendation : approval
compensation : pay
complain : gripe
complaisant : broad-minded
completely : totally
complete : whole
compliant : willing
comprehension : understanding
comprise : include
concerning : about
condole : comfort
connote : imply
consecrate : dedicate
consequential : important
consequently : so
construct : build
consume : use
contiguous : neighboring
contract : go down with
contribute : give
convene : gather
convey : bring
copious : abundant
corpulent : fat
correspondence : letter
could not : couldn't
counsel : advise
craven : cowardly
criterion : yardstick
crux : gist
cursory : slapdash
cynical : jaundiced
data : facts
decline : drop
decompose : decay
decrease : shorten
deem : consider
deficiency : lack
dehydrated : parched
deluge : swamp
demonstrate : show
denote : meant
denouement : outcome
depart : go
depart : leave
deportment : conduct
desiccated : dry
designate : assign
designate : dub
desire : want
desist : cease
detect : learn
dialogue : chat
diaphanous : filmy
did not : didn't
diminutive : small
dine : eat
diplomatic : tactful
directive : command
disaffection : loneliness
disarray : confusion
discomposure : embarrassement
discover : find
dishonorable : shameful
disinterested : fair
dissatisfied : unhappy
distribute : give out
disturb : upset
diurnal : daily
does not : doesn't
donate : give
do not : don't
drunkard : boozer
dubiety : doubt
dullard : numbskull
dwelling : house
educate : teach
efface : erase
effulgent : shining
elevated : high
elucidate : explain
emanate : originate
emolument : stipend
emotion : feeling
employ : take on
endeavour : try
energetic : lively
enormous : huge
enquire : ask about
ensure : make sure
entire : whole
eradicate : cancel
eschew : avoid
essentially : pretty much
eternal : timeless
ethical : decent
everyone : everybody
evince : show
exasperate : bug
exempt : free
exiguous : scanty
expedite : hurry
expunge : delete
extort : rob
facsimile : duplicate
father : dad
fatigued : tired
feasible : workable
felicity : happiness
finally : in the end
finish : end
flavorful : delicious
fondle : cuddle
fool : chump
foreboding : dread
fortitude : resolution
fortunate : lucky
frangible : fragile
friend : crony
fundamental : basic
furthermore : and
gainsay : oppose
gastronome : gourmet
gaudy : flashy
generosity : goodwill
gift : present
gladden : encourage
govern : control
gratuity : tip
greater : bigger
had not : hadn't
halt : stop
handle : deal with
has not : hasn't
have not : haven't
he had : he'd
he has : he's
he is : he's
he shall : he'll
he will : he'll
he would : he'd
highly : very
hound : dog
however : but
humankind : humanity
I am : I'm
I had : I'd
I have : I've
ill : awful
illimitable : boundless
illuminate : lit up
imbibe : digest
immaculate : clean
immature : childish
immediately : at once
immerse : dunk
immutable : unchangeable
impact : jolt
impact : shock
impel : drive
in addition : and
incapacitate : disable
incorrect : wrong
increase : go up
indistinct : dim
indolence : laziness
inexorable : relentless
inexpensive : cheap
inferior : worse
infirmity : illness
informer : blabbermouth
inform : tell
ingest : abstracted
initially : at first
innate : inbred
in other words : mean
insane : mad
insinuation : hint
insolvent : broke
instrumentality : means
intend : aim
intermittently : on and off
interpose : interject
interrogate : grill
intoxicated : drunk
intractable : unruly
investigate : check up
I shall : I'll
is not : isn't
issue : thing
items : stuff
it had : it'd
it is : it's
it would : it'd
I will : I'll
I would : I‘d
jeopardy : risk
jocular : humorous
joke : wisecrack
jurisdiction : authority
kiss : smooch
knife : shiv
laborious : hard
larceny : stickup
large : big
laud : praise
leave : get out
let us : let's
lie : fib
littoral : beach
locate : find
lonely : lonesome
luminous : gleaming
magnanimous : generous
major : big
malady : disease
manage : put up
man : guy
mannered : arty
masticate : chew
materials : stuff
matter : thing
mature : ripen
maudlin : mushy
mawkish : slushy
meager : skimpy
mediocre : so-so
mesomorphic : fleshy
midst : middle
might not : mightn't
mimicry : takeoff
mindful : conscious
minimize : belittle
minister : preacher
mistake : goof
mother : mom
motivate : prompt
multiply : times
must : have to
must not : mustn't
narrative : story
nefarious : evil
negligible : piddling
nervous : uptight
no one : nobody
novice : beginner
numerous : many
objective : goal
observe : watch
obtain : get
obtuse : dumb
occupation : job
occupied : busy
opportunity : chance
oration : speech
ornament : trim
oscillate : swing
palatable : tasty
parallel : resemble
paramount : outstanding
pardon : forgive
partake : participate
passage : portion
pedantic : nitpicking
pedantry : learning
penetrate : stab
perform : do
perform : go through
perhaps : maybe
permit : allow
permit : let
perspiration : sweat
petition : appeal
photograph : photo
plead : beg
pleonastic : redundant
poignant : touching
ponderous : hefty
portly : chubby
possess : own
possibly : maybe
postulate : assume
potato : spud
prescience : farsightedness
presentiment : premonition
preserve : keep
pretty : cute
previous : earlier
principally : mainly
procedure : method
proffer : offer
propagate : spread
prosperous : loaded
prying : nosy
purchase : buy
quite : really
quote : quotation
quotidian : everyday
rapid : quick
Rather : sort of
ratiocination : reasoning
recall : remember
recount : tell
rectify : fix
reduce : shorten
refrigerator : fridge
reject : say no
relax : lay back
release : free
remit : excuse
renaissance : rebirth
renowned : famous
repair : mend
repeatedly : again and again
replica : copy
repose : rest
reprehension : blame
repugnant : disgusting
reputable : great
request : setup
require : need
requisition : application
research : look up
reside : live
residence : house
residue : leftover
resign : quit
respite : letup
resplendent : brilliant
responsible : in charge
restless : edgy
retain : keep
return : send back
risque : raunchy
sanguinary : bloody
sanguine : optimistic
satisfactory : ok
scheme : plan
scholar : learner
seize : snatch
select : choose
senior : older
sepulcher : tomb
shall not : shan't
she had : she'd
she has : she's
she is : she's
she shall : she'll
she will : she'll
she would : she'd
should not : shouldn't
skeptic : doubter
social : sociable
solace : console
solicitude : concern
someone : somebody
somewhat : kind of
song : tune
specialization : field
spectacles : glasses
spoil : rot
sportive : frisky
stalwart : strong
submerge : douse
subsequently : next
such as : like
sufficient : enough
summit : peak
sunder : cleave
sundry : various
superior : better
supersede : replace
sway : power
swift : Fast
talkative : gabby
telephone : phone
tepid : lukewarm
terminate : end
terrorize : bully
that has : that's
that is : that's
that will : that'll
therefore : so
there has : there's
there is : there's
they are : they're
they had : they'd
they have : they've
they shall : they'll
they will : they'll
they would : they'd
things : stuff
throng : crowd
tillage : gardening
transfer : move
transform : change
transmit : send
transmute : convert
transparent : clear
transport : carry
type : sort
underhand : sneaky
understand : catch on
undertaking : project
unending : endless
unending : everlasting
unmistakable : clear
unsubstantial : flimsy
vacant : empty
valiant : Plucky
vanity : conceit
vaunt : brag
velocity : speed
veracious : truthful
verbose : wordy
virtuous : moral
vision : sight
vomit : bring up
voyage : trip
wail : cry
wan : pale
we are : we're
we had : we'd
we have : we've
were not : weren't
we shall : we'll
we will : we'll
we would : we'd
what are : what're
what has : what's
what have : what've
what is : what's
what shall : what'll
what will : what'll
where has : where's
where is : where's
whine : bellyache
who are : who're
who had : who's
who has : who's
who have : who've
who is : who's
who shall : who'll
who will : who'll
who would : who's
will : be going to
will not : won't
wish : want
woman : lady
would have : would've
would not : wouldn't
you are : you're
you had : you'd
you have : you've
you shall : you'll
you will : you'll
you would : you'd

#-*- coding: utf-8 -*-
#to compile : python -Qnew easyConversion.py

import textatistic

class Conversion(object):

	def __init__(self):
		self.dic = {}
		with open('easyDic.txt') as f:
			for line in f:
				k, v = line.strip().split(' : ')
				self.dic[k.strip()] = v.strip()
		f.close()
		
		self.corpus = {}
		with open('texts.txt') as g:
			for line in g:
				k, v = line.strip().split(' : ')
				self.corpus[k.strip()] = v.strip()
		g.close()
		
		self.flesch = {}
		self.dalechall = {}
		self.smog = {}
		self.average_flesch = 0
		self.average_dalechall = 0
		self.average_smog = 0
		count = 0
		for i, j in self.corpus.items():
			count += 1
			original = textatistic.Textatistic(j)
			self.flesch[i] = original.flesch_score
			self.dalechall[i] = original.dalechall_score
			self.smog[i] = original.smog_score
			self.average_flesch += self.flesch[i]
			self.average_dalechall += self.dalechall[i]
			self.average_smog += self.smog[i]
		self.average_flesch = self.average_flesch/count
		self.average_dalechall = self.average_dalechall/count
		self.average_smog = self.average_smog/count
					
		self.converted = {}
		self.newflesch = {}
		self.newDalechall = {}
		self.newSmog = {}
		self.average_new_flesch = 0
		self.average_new_dalechall = 0
		self.average_new_smog = 0
		self.gap_flesch = 0
		self.gap_dalechall = 0
		self.gap_smog = 0
		
	#Convert every text in the formal texts corpus and calculate readibility scores, average scores and gaps
	def convertAll(self):
		count = 0
		for i, j in self.corpus.items():
			count += 1
			self.converted[i] = self.convert(j)
			convertedText = textatistic.Textatistic(self.converted[i])
			self.newflesch[i] = convertedText.flesch_score
			self.newDalechall[i] = convertedText.dalechall_score
			self.newSmog[i] = convertedText.smog_score
			self.average_new_flesch += self.newflesch[i]
			self.average_new_dalechall += self.newDalechall[i]
			self.average_new_smog += self.newSmog[i]
			self.gap_flesch += abs(self.flesch[i]-self.newflesch[i])
			self.gap_dalechall += abs(self.dalechall[i]-self.newDalechall[i])
			self.gap_smog += abs(self.smog[i]-self.newSmog[i])
		self.average_new_flesch = self.average_new_flesch/count
		self.average_new_dalechall = self.average_new_dalechall/count
		self.average_new_smog = self.average_new_smog/count
		self.gap_flesch = self.gap_flesch/count
		self.gap_dalechall = self.gap_dalechall/count
		self.gap_smog = self.gap_smog/count

	#Convert formal texts into more easily readable texts
	def convert(self, text):
		for i, j in self.dic.items():
		    text = text.replace(i, j)
		return text
		
	#Print the dictionary
	def printDic(self):
		for k, v in self.dic.items():
			print(k)
			print(v)
			
	#Print results of Conversion
	def printAll(self):
		for i, j in self.corpus.items():
		
			print("ORIGINAL TEXT:\n")
			print "Text: %s" % j
			print( "flesch score: {}".format(self.flesch[i]))
			print("Dale-Chall score: {}".format(self.dalechall[i]))
			print("SMOG score: {}".format(self.smog[i]))
			print("\n")

			print("CONVERTED TEXT:\n")
			print "Text: %s" % self.converted[i]
			print( "flesch score: {}".format(self.newflesch[i]))
			print("Dale-Chall score: {}".format(self.newDalechall[i]))
			print("SMOG score: {}".format(self.newSmog[i]))
			print("\n")
			
	#Print the average scores and gap
	def printAverage(self):
		print("ORIGINAL TEXTS - AVERAGE:\n")
		print( "flesch score: {}".format(self.average_flesch))
		print("Dale-Chall score: {}".format(self.average_dalechall))
		print("SMOG score: {}".format(self.average_smog))
		print("\n")
		
		print("CONVERTED TEXTS - AVERAGE:\n")
		print( "flesch score: {}".format(self.average_new_flesch))
		print("Dale-Chall score: {}".format(self.average_new_dalechall))
		print("SMOG score: {}".format(self.average_new_smog))
		print("\n")
		
		print("AVERAGE GAPS:\n")
		print( "For flesch score: {}".format(self.gap_flesch))
		print("For Dale-Chall score: {}".format(self.gap_dalechall))
		print("For SMOG score: {}".format(self.gap_smog))
		print("\n")

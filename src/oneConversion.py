#-*- coding: utf-8 -*-
#to compile : python -Qnew oneConversion.py

import easyConversion
import textatistic

c = easyConversion.Conversion()

#Add your text here
text = ""

#Conversion of the text
convertedText = c.convert(text)

#Calculations of readibility scores
#You can comment the according parts below if textatistic doesn't work

original = textatistic.Textatistic(text)
converted = textatistic.Textatistic(convertedText)

print("ORIGINAL TEXT:\n")
print "Text: %s" % text
print( "Flesh score: {}".format(original.flesch_score))
print("Dale-Chall score: {}".format(original.dalechall_score))
print("Gunning Fog score: {}".format(original.gunningfog_score))
print("SMOG score: {}".format(original.smog_score))
print("\n")

print("CONVERTED TEXT:\n")
print "Text: %s" % convertedText
print( "Flesh score: {}".format(converted.flesch_score))
print("Dale-Chall score: {}".format(converted.dalechall_score))
print("Gunning Fog score: {}".format(converted.gunningfog_score))
print("SMOG score: {}".format(converted.smog_score))
print("\n")
